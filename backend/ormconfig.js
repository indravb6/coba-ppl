module.exports = {
  type: "postgres",
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  entities: ["src/**/*.entity.ts", "src/**/*.entity.js"],
  migrationsTableName: "migration_table",
  migrations: ["migrations/*.ts"],
  cli: { migrationsDir: "migrations" }
};
