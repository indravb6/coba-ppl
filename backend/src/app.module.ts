import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { DebugController } from "./services/debug/debug.controller";

@Module({
  imports: [TypeOrmModule.forRoot(), TypeOrmModule.forFeature([])],
  controllers: [DebugController],
  providers: []
})
export class AppModule {}
