import { Controller, Get } from "@nestjs/common";

import { Response } from "../../common/models/response";

@Controller("/debug")
export class DebugController {
  @Get("/hello-world")
  helloWorld(): Response {
    return new Response("Hello World!");
  }
}
