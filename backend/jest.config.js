module.exports = {
  roots: ["<rootDir>/test"],
  transform: { "^.+\\.ts$": "ts-jest" },
  testEnvironment: "node",
  testResultsProcessor: "jest-sonar-reporter",
  collectCoverageFrom: ["src/**/*.ts"]
};
