import request from "supertest";
import { Test } from "@nestjs/testing";
import { INestApplication, Controller, Post, Body } from "@nestjs/common";
import { IsInt, Max, Min, IsString, ValidateNested } from "class-validator";
import { Type } from "class-transformer";

import { ValidationPipe } from "../../../src/common/middleware/validation.pipe";

class DummyNameForm {
  @IsString()
  first: string;

  @IsString()
  last: string;
}

class DummyForm {
  @IsInt()
  @Min(10)
  @Max(30)
  age: number;

  @ValidateNested()
  @Type(() => DummyNameForm)
  name: DummyNameForm;
}

@Controller("/dummy")
class DummyController {
  @Post("/save")
  save(@Body() _: DummyForm) {}
}

describe("Test Validation Pipe", () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [DummyController]
    }).compile();

    app = moduleRef.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  test(`Send expected form`, () => {
    return request(app.getHttpServer())
      .post("/dummy/save")
      .send({ name: { first: "andi", last: "budi" }, age: 17 })
      .expect(201);
  });

  test(`Send invalid constraint`, () => {
    return request(app.getHttpServer())
      .post("/dummy/save")
      .send({ name: { first: "andi", last: "budi" }, age: 1 })
      .expect(400)
      .expect({ age: "age must not be less than 10" });
  });

  test(`Send invalid constraint on nested object`, () => {
    return request(app.getHttpServer())
      .post("/dummy/save")
      .send({ name: { first: 1, last: "budi" }, age: 17 })
      .expect(400)
      .expect({ name: { first: "first must be a string" } });
  });

  afterAll(async () => {
    await app.close();
  });
});
