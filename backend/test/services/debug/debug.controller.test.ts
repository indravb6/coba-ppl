import { DebugController } from "../../../src/services/debug/debug.controller";

describe("Debug Controller", () => {
  let debugController: DebugController;

  beforeEach(() => {
    debugController = new DebugController();
  });

  describe("HelloWorld()", () => {
    test("return success", () => {
      const response = debugController.helloWorld();

      expect(response.message).toEqual("Hello World!");
    });
  });
});
