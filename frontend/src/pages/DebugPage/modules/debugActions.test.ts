import { debugActions } from "./debugActions";

describe("Debug Actions", () => {
  let dispatch: jest.Mock<any>;
  let getState: jest.Mock<any>;

  let debugAPI: jest.Mocked<any>;

  beforeEach(() => {
    dispatch = jest.fn();
    getState = jest.fn();

    debugAPI = {
      helloWorld: jest.fn()
    };
  });

  describe("Hello World", () => {
    test("Calls API to get hello world message", async () => {
      debugAPI.helloWorld = jest.fn().mockResolvedValue({ message: "Hello World!" });
      const response = await debugActions.helloWorld()(dispatch, getState, { debugAPI });

      expect(debugAPI.helloWorld).toHaveBeenCalled();
      expect(response).toEqual("Hello World!");
    });
  });
});
