export const debugActions = {
  helloWorld: () => {
    return async (dispatch, getState, { debugAPI }) => {
      const response = await debugAPI.helloWorld();

      return response.message;
    };
  }
};
