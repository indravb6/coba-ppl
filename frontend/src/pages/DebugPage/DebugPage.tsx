import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import { debugActions as injectedDebugActions } from "./modules/debugActions";

interface DebugPageProps {
  onHelloWorld: () => Promise<string>;
}

const DebugPage = (props: DebugPageProps) => {
  const [text, setText] = useState("");

  useEffect(() => {
    props.onHelloWorld().then(setText);
  }, []);

  return <>{text}</>;
};

export function createDebugPage(debugActions) {
  const mapDispatchToProps = {
    onHelloWorld: debugActions.helloWorld
  };

  return connect(undefined, mapDispatchToProps)(DebugPage);
}

export default createDebugPage(injectedDebugActions);
