import { mount, ReactWrapper } from "enzyme";
import React from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router";
import thunk from "redux-thunk";
import configureStore from "redux-mock-store";

import { createDebugPage } from "./DebugPage";

describe("DebugPage", () => {
  let debugActions: jest.Mocked<any>;
  let wrapper: ReactWrapper<any, any>;
  const middlewares = [thunk];
  const mockStore = configureStore(middlewares);

  beforeEach(() => {
    debugActions = {
      helloWorld: jest.fn().mockReturnValue(() => Promise.resolve("Hello World!"))
    };

    const DebugPage = createDebugPage(debugActions);

    wrapper = mount(
      <Provider store={mockStore({})}>
        <MemoryRouter>
          <DebugPage />
        </MemoryRouter>
      </Provider>
    );
  });

  test("Renders hello world text", async () => {
    await new Promise(resolve => setImmediate(resolve));
    wrapper.update();

    expect(debugActions.helloWorld).toHaveBeenCalled();
    expect(wrapper.text()).toContain("Hello World");
  });
});
