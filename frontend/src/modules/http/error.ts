class HttpError {
  name = "HttpError";
  message: string;
  stack?: string;

  constructor(body?: SerializableError) {
    const error = new Error(body && body.message);
    this.stack = error.stack;
    this.message = error.message;
  }
}

export class BadRequestError extends HttpError {
  name = "BadRequestError";
  constructor(body?: SerializableError) {
    super({ message: (body && body.message) || "There's something wrong" });
  }
}

export class UnauthorizedError extends HttpError {
  name = "UnauthorizedError";
}

export class ForbiddenError extends HttpError {
  name = "ForbiddenError";
}

export class NotFoundError extends HttpError {
  name = "NotFoundError";
}

export class RemoteError extends HttpError {
  name = "RemoteError";
}

export interface SerializableError {
  message: string;
}
