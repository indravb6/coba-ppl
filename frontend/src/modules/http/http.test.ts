import { get, post, delete_, put, patch, postMultipart } from "./http";
import { BadRequestError, UnauthorizedError, ForbiddenError, NotFoundError, RemoteError } from "./error";

describe("HTTP", () => {
  afterAll(() => {
    jest.clearAllMocks();
  });

  describe("OK response", () => {
    test("GET request", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(JSON.stringify({ message: "hello" }), { status: 200 }));

      const response = await get("localhost:3001");

      expect(window.fetch).toHaveBeenCalledTimes(1);
      expect(window.fetch).toHaveBeenCalledWith("localhost:3001", {
        body: undefined,
        headers: {},
        method: "GET",
        mode: "cors"
      });
      expect(response).toEqual({ message: "hello" });
    });

    test("DELETE request", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(JSON.stringify({ message: "hello" }), { status: 200 }));

      const response = await delete_("localhost:3001", "ABC");

      expect(window.fetch).toHaveBeenCalledTimes(1);
      expect(window.fetch).toHaveBeenCalledWith("localhost:3001", {
        body: undefined,
        headers: { Authorization: "Bearer ABC" },
        method: "DELETE",
        mode: "cors"
      });
      expect(response).toEqual({ message: "hello" });
    });

    test("POST request", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(JSON.stringify({ message: "hello" }), { status: 201 }));

      const response = await post("localhost:3001", "ABC", { data: 1 });

      expect(window.fetch).toHaveBeenCalledTimes(1);
      expect(window.fetch).toHaveBeenCalledWith("localhost:3001", {
        body: JSON.stringify({ data: 1 }),
        headers: { Authorization: "Bearer ABC", "Content-Type": "application/json" },
        method: "POST",
        mode: "cors"
      });
      expect(response).toEqual({ message: "hello" });
    });

    test("PUT request", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(JSON.stringify({ message: "hello" }), { status: 200 }));

      const response = await put("localhost:3001", "ABC", { data: 1 });

      expect(window.fetch).toHaveBeenCalledTimes(1);
      expect(window.fetch).toHaveBeenCalledWith("localhost:3001", {
        body: JSON.stringify({ data: 1 }),
        headers: { Authorization: "Bearer ABC", "Content-Type": "application/json" },
        method: "PUT",
        mode: "cors"
      });
      expect(response).toEqual({ message: "hello" });
    });

    test("PATCH request", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(JSON.stringify({ message: "hello" }), { status: 200 }));

      const response = await patch("localhost:3001", "ABC", { data: 1 });

      expect(window.fetch).toHaveBeenCalledTimes(1);
      expect(window.fetch).toHaveBeenCalledWith("localhost:3001", {
        body: JSON.stringify({ data: 1 }),
        headers: { Authorization: "Bearer ABC", "Content-Type": "application/json" },
        method: "PATCH",
        mode: "cors"
      });
      expect(response).toEqual({ message: "hello" });
    });

    test("POST request with file", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(JSON.stringify({ message: "hello" }), { status: 201 }));
      const data = { data: 1, file: "q" };

      const response = await postMultipart("localhost:3001", "ABC", data);

      expect(window.fetch).toHaveBeenCalledTimes(1);

      const formData = new FormData();
      Object.keys(data).forEach(part => formData.append(part, data[part]));
      expect(window.fetch).toHaveBeenCalledWith("localhost:3001", {
        body: formData,
        headers: { Authorization: "Bearer ABC" },
        method: "POST",
        mode: "cors"
      });

      expect(response).toEqual({ message: "hello" });
    });
  });

  describe("Error response", () => {
    test("Throws BadRequestError", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(JSON.stringify({ message: "hello" }), { status: 400 }));

      try {
        await get("localhost:3001", "ABC");
      } catch (e) {
        expect(e).toBeInstanceOf(BadRequestError);
        expect(e.message).toEqual("hello");
      }
    });

    test("Throws BadRequestError with default message", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(null, { status: 400 }));

      try {
        await get("localhost:3001", "ABC");
      } catch (e) {
        expect(e).toBeInstanceOf(BadRequestError);
        expect(e.message).toEqual("There's something wrong");
      }
    });

    test("Throws UnauthorizedError", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(JSON.stringify({ message: "hello" }), { status: 401 }));

      try {
        await get("localhost:3001", "ABC");
      } catch (e) {
        expect(e).toBeInstanceOf(UnauthorizedError);
        expect(e.message).toEqual("");
      }
    });

    test("Throws ForbiddenError", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(JSON.stringify({ message: "hello" }), { status: 403 }));

      try {
        await get("localhost:3001", "ABC");
      } catch (e) {
        expect(e).toBeInstanceOf(ForbiddenError);
        expect(e.message).toEqual("hello");
      }
    });

    test("Throws NotFoundError", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(JSON.stringify({ message: "hello" }), { status: 404 }));

      try {
        await get("localhost:3001", "ABC");
      } catch (e) {
        expect(e).toBeInstanceOf(NotFoundError);
        expect(e.message).toEqual("hello");
      }
    });

    test("Throws RemoteError", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response(JSON.stringify({ message: "hello" }), { status: 500 }));

      try {
        await get("localhost:3001", "ABC");
      } catch (e) {
        expect(e).toBeInstanceOf(RemoteError);
        expect(e.message).toEqual("hello");
      }
    });
  });

  describe("Can't fetch", () => {
    test("Throws RemoteError", async () => {
      window.fetch = jest.fn().mockRejectedValue({});

      try {
        await get("localhost:3001", "ABC");
      } catch (e) {
        expect(e).toBeInstanceOf(RemoteError);
        expect(e.message).toEqual("");
      }
    });
  });

  describe("Bad json format", () => {
    test("Return undefined", async () => {
      window.fetch = jest.fn().mockResolvedValue(new Response("s", { status: 200 }));

      const response = await get("localhost:3001", "ABC");
      expect(response).toBeUndefined();
    });
  });
});
