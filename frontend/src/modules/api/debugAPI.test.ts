import { debugAPI } from "./debugAPI";
import * as http from "./../http/http";

describe("Debug API", () => {
  afterAll(() => {
    jest.clearAllMocks();
  });

  test("Hello World", async () => {
    const getMock = jest.spyOn(http, "get");
    getMock.mockResolvedValue({ message: "Hello World!" });

    const response = await debugAPI.helloWorld();

    expect(getMock).toHaveBeenCalledTimes(1);
    expect(getMock).toHaveBeenCalledWith(`${http.BASE_API}/debug/hello-world`);
    expect(response).toEqual({ message: "Hello World!" });
  });
});
