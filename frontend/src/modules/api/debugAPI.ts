import { Message, get, BASE_API } from "../http/http";

const baseURL = `${BASE_API}/debug`;

export const debugAPI = {
  helloWorld: (): Promise<Message> => {
    return get(`${baseURL}/hello-world`);
  }
};
