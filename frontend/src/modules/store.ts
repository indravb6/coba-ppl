import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { createBrowserHistory } from "history";
import { connectRouter, routerMiddleware } from "connected-react-router";
import { persistStore } from "redux-persist";

import { debugAPI } from "./api/debugAPI";

export const history = createBrowserHistory();

const rootReducer = combineReducers({
  router: connectRouter(history)
});

export type AppState = ReturnType<typeof rootReducer>;

export const store = createStore(
  rootReducer,
  applyMiddleware(
    thunk.withExtraArgument({
      debugAPI
    }),
    routerMiddleware(history)
  )
);

export const persistor = persistStore(store);
