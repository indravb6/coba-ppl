import React from "react";
import { Route, Switch } from "react-router-dom";

import DebugPage from "./pages/DebugPage/DebugPage";

const App = () => {
  return (
    <Switch>
      <Route path="/" component={DebugPage} />
    </Switch>
  );
};

export default App;
