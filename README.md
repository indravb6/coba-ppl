# Logbook Elektronik

Pencatatan kegiatan lapangan para peserta didik (dokter) yang nantinya dilaporkan kepada para Konsulen secara online.

**Backend:** ![build](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/coding-with-us-fk-ui-logbook-elektronik/badges/staging/pipeline.svg?job=backend-test&style=flat-square) ![coverage](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/coding-with-us-fk-ui-logbook-elektronik/badges/staging/coverage.svg?job=backend-test&style=flat-square)\
**Frontend:** ![build](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/coding-with-us-fk-ui-logbook-elektronik/badges/staging/pipeline.svg?job=frontend-test&style=flat-square) ![coverage](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/coding-with-us-fk-ui-logbook-elektronik/badges/staging/coverage.svg?job=frontend-test&style=flat-square)

### Requirement :

- NodeJS 10.15
- Yarn
- PostgreSQL

## Setup Backend Environment

1.  Go inside backend directory:\
    `cd backend`
2.  Copy the example config:\
    `cp env.sample .env`
3.  Preparing local database:\
    a. Create new database called `logbook`\
    b. Open the config file `.env`\
    c. modify `DB_HOST`, `DB_PORT`, `DB_USER` and `DB_PASS` as necessary

4.  Install the dependencies:\
    `yarn`
5.  Run DB migrations:\
    `yarn migration:run`
6.  Run:  
    `yarn test`\
    Make sure the tests pass before moving on to the next sections.

Start Backend: `yarn start`

## Setup Frontend Environment

1.  Go inside frontend directory:\
    `cd frontend`
2.  Copy the example config:\
    `cp env.sample .env`
3.  Install the dependencies:\
    `yarn`
4.  Run:  
     `yarn test`\
     Make sure the tests pass before moving on to the next sections.

Start Frontend: `yarn start`

## Start project with docker

`docker-compose up`

## Developers

Coding With Us - Tim A2 PPL 2020

- Gema Pratama Aditya - 1706040031
- Muhammad Indra Ramadhan - 1706028695
- Rahmat Fadhilah - 1706074902
- Rizkhi Pramudya Hastiputra - 1706039660
- Shafira Ishlah Nurulita - 1706979474
